<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>意向跟进</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveMasterRecord(){
	if (validate()){
		if (ele("currentSubTableId")){
			var subTableId = $("#currentSubTableId").val();
			if (!checkEntryRecords(subTableId)){
				return;
			}
		}
		showSplash();
		postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
			if ("fail" != responseText){
				$('#operaType').val('update');
				$('#TASK_ID').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});
	}
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
function checkEntryRecords(subTableId){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
	return result;
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'350px',top:'10px',scroll:'yes'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}
function deleteSubRecord(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要删除该条记录吗？',function(r){
		if(r){
			doSubmit({actionType:'deleteSubRecord'});
		}
	});
}
function doMoveUp(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	doSubmit({actionType:'moveDown'});
}
var operaPostponeTaskBox;
function openPostponeBox(){
	
	var url = "<%=pageBean.getHandlerURL()%>&actionType=checkBusinessOrders&CUST_ID="+$('#CUST_ID').val();
	sendRequest(url,{onComplete:function(responseText){
		if(responseText == 'success'){
			var handlerId = "PostponeManage";
			if (!operaPostponeTaskBox){
				operaPostponeTaskBox = new PopupBox('operaPostponeTaskBox','无意向',{size:'normal',width:'600',top:'2px',scroll:'yes'});
			}
			var url = 'index?'+handlerId+'&ORG_ID='+$('#ORG_ID').val()+'&TASK_ID='+$('#TASK_ID').val()+'&CUST_ID='+$('#CUST_ID').val()+'&TASK_REVIEW_ID='+$('#TASK_REVIEW_ID').val()+'&CUST_NAME='+$('#CUST_NAME').val();
			operaPostponeTaskBox.sendRequest(url);
		}else{
			jAlert('此客户已存在商机或订单记录，不能变为潜在客户！');
		}
	}});
}
var operaFollowUpTaskBox;
function openFollowUpBox(){
	var handlerId = "FollowUpManage";
	if (!operaFollowUpTaskBox){
		operaFollowUpTaskBox = new PopupBox('operaFollowUpTaskBox','<%=pageBean.inputValue("buttonName")%>',{size:'big',width:'850',height:'600',top:'2px',scroll:'yes'});
	}
	var url = 'index?'+handlerId+'&VISIT_CUST_ID='+$('#CUST_ID').val()+'&TASK_ID='+$('#TASK_ID').val()+'&ORG_ID='+$('#ORG_ID').val()+'&TASK_REVIEW_ID='+$('#TASK_REVIEW_ID').val();
	operaFollowUpTaskBox.sendRequest(url);
}
function refreshCitySelect(){
	var url = "<%=pageBean.getHandlerURL()%>&actionType=refreshCitySelect&CUST_PROVINCE="+$('#CUST_PROVINCE').val();
	sendRequest(url,{onComplete:function(responseText){
		$("#CUST_CITY").html(responseText);
	}});
}
function goBack(){
	doSubmit({actionType:'goBack'});
}
function goBackId(reviewId){
	$('#TASK_REVIEW_ID').val(reviewId);
	doSubmit({actionType:'goBack'});
}
var openCustInfoEditBox;
function openCustInfoBox(){
	var handlerId = "MyTaskCustInfoEdit";
	if (!openCustInfoEditBox){
		openCustInfoEditBox = new PopupBox('openCustInfoEditBox','我的客户',{size:'big',width:'870',height:'600',top:'2px',scroll:'yes'});
	}
	var url = 'index?'+handlerId+'&CUST_ID='+$('#CUST_ID').val();
	openCustInfoEditBox.sendRequest(url);
}
function doRefresh(){
	doSubmit({actionType:'prepareDisplay'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top:7px;">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeSubTable('_base')">基础信息</div>
<div class="newarticle1" onclick="changeSubTable('MyCustVisitInfo')">拜访记录</div>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">
<div style="margin:2px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
<%if (!"ConfirmSummary".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openPostponeBox()" ><input value="&nbsp;" type="button" class="editImgBtn" id="postponeImgBtn" title="无意向" />无意向</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openFollowUpBox()"><input value="&nbsp;" type="button" class="saveImgBtn" id="followUpImgBtn" title="跟进" /><%=pageBean.inputValue("buttonName")%></td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openCustInfoBox()"><input value="&nbsp;" type="button" class="editImgBtn" id="postponeImgBtn" title="编辑客户" />编辑客户</td>
<%} %>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input name="CUST_NAME" type="text" class="text" id="CUST_NAME" value="<%=pageBean.inputValue("CUST_NAME")%>" size="24" label="名称" />
    </td>
</tr>    
<tr>
	<th width="100" nowrap>行业</th>
	<td><select id="CUST_INDUSTRY" label="行业" name="CUST_INDUSTRY" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_INDUSTRY")%></select>
</tr>
<tr>
  	<th width="100" nowrap>公司网站</th>
	<td><input name="CUST_COMPANY_WEB" type="text" class="text" id="CUST_COMPANY_WEB" value="<%=pageBean.inputValue("CUST_COMPANY_WEB")%>" size="24" label="名称" />
</td>
</tr>
<tr>
	<th width="100" nowrap>规模</th>
	<td><select id="CUST_SCALE" label="规模" name="CUST_SCALE" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_SCALE")%></select>
</td>
</tr>    
<tr>
	<th width="100" nowrap>性质</th>
	<td><select id="CUST_NATURE" label="性质" name="CUST_NATURE" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_NATURE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>级别</th>
	<td>
    <select id="CUST_LEVEL" label="级别" name="CUST_LEVEL" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_LEVEL")%></select>
</td>
</tr>    
<tr>
	<th width="100" nowrap>进展状态</th>
	<td>
    <select id="CUST_PROGRESS_STATE" label="进展状态" name="CUST_PROGRESS_STATE" class="select edit-selcct-size"><%=pageBean.selectValue("CUST_PROGRESS_STATE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>电话</th>
	<td><input id="CUST_PHONE" label="电话" name="CUST_PHONE" type="text" value="<%=pageBean.inputValue("CUST_PHONE")%>" style="width:504px;" size="65" class="text" />
</td>
</tr> 
<tr>
	<th width="100" nowrap>地址</th>
	<td><input id="CUST_ADDRESS" label="地址" name="CUST_ADDRESS" type="text" value="<%=pageBean.inputValue("CUST_ADDRESS")%>" style="width:504px;" size="65" class="text" />
</td>
</tr>    
<tr>
	<th width="100" nowrap>状态</th>
	<td>
    <input id="STATE_TEXT" label="状态" name="STATE_TEXT" type="text" value="<%=pageBean.selectedText("CUST_STATE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="CUST_STATE" label="状态" name="CUST_STATE" type="hidden" value="<%=pageBean.selectedValue("CUST_STATE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>简要介绍</th>
	<td><textarea id="CUST_INTRODUCE" label="简要介绍" name="CUST_INTRODUCE" cols="60" rows="6" class="textarea" style="width:500px;"><%=pageBean.inputValue("CUST_INTRODUCE")%></textarea>
</td>
</tr>
</table>
</div>
</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<%if ("MyCustVisitInfo".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewSubRecordRequest('update','拜访记录','MyCustVisitInfo','PROCUST_VISIT_ID')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>      
</tr>   
   </table>
</div>
<div style="margin:2px;">
<%
List param1Records = (List)pageBean.getAttribute("MyCustVisitInfoRecords");
pageBean.setRsList(param1Records);
%>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="CRM_CUSTOMER_INFO.csv"
retrieveRowsCallback="process" xlsFileName="CRM_CUSTOMER_INFO.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="auto"
>
<ec:row styleClass="odd" ondblclick="viewSubRecordRequest('detail','拜访记录','MyCustVisitInfo','PROCUST_VISIT_ID')" onclick="selectRow(this,{PROCUST_VISIT_ID:'${row.PROCUST_VISIT_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="PROCUST_VISIT_DATE" title="拜访日期" cell="date" format="yyyy-MM-dd" />
	<ec:column width="100" property="PROCUST_VISIT_FILL_NAME" title="填写人" />
	<ec:column width="100" property="PROCUST_VISIT_FILL_TIME" title="填写时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="PROCUST_VISIT_EFFECT" title="拜访效果"   mappingItem="PROCUST_VISIT_EFFECT"/>
	<ec:column width="50" property="PROCUST_VISIT_TYPE" title="拜访类型" mappingItem="PROCUST_VISIT_TYPE"/>
</ec:row>
</ec:table>
</div>
</div>
<script language="javascript">
setRsIdTag('PROCUST_VISIT_ID');
</script>
<%}%>
<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
<script language="javascript">
$("#Layer0").hide();
</script>
<%}%>
<%}%>
<script language="javascript">
new Tab('tab','tabHeader','Layer',<%=currentSubTableIndex%>);
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
$("#Layer0").hide();
<%}%>
</script>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="TASK_ID" name="TASK_ID" value="<%=pageBean.inputValue4DetailOrUpdate("TASK_ID","")%>" />
<input type="hidden" name="CUST_ID" id="CUST_ID" value="<%=pageBean.inputValue("CUST_ID")%>"/>
<input type="hidden" name="PROCUST_VISIT_ID" id="PROCUST_VISIT_ID" value="<%=pageBean.inputValue("PROCUST_VISIT_ID")%>"/>
<input type="hidden" name="ORG_ID" id="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>"/>
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
</div>
</form>
<script language="javascript">
initDetailOpertionImage();
$(function(){
	resetTabHeight(80);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
