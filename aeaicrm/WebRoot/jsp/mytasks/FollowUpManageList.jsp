<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>意向跟进</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var custIdBox;
function openCustIdBox(){
	var handlerId = "MyCustomerSelectList"; 
	if (!custIdBox){
		custIdBox = new PopupBox('custIdBox','请选择我的客户',{size:'normal',width:'600',top:'2px'});
	}
	var reviewId = $('#TASK_REVIEW_ID').val();
	var url = 'index?'+handlerId+'&targetId=CUST_ID&TASK_REVIEW_ID='+reviewId;
	custIdBox.sendRequest(url);
}
function createTask(){
	$('#ids').val();
	$('#TASK_REVIEW_ID').val();
	doSubmit({actionType:'createTask'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolBar" border="0" cellpadding="0" cellspacing="1">
<tr>
<%if (!"ConfirmSummary".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="openCustIdBox()"><input value="&nbsp;" title="创建" type="button" class="createImgBtn" />创建</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
<%} %>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
<%if ("Init".equals(pageBean.inputValue("TASK_REVIEW_STATE"))){%>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
<%} %>   
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="意向跟进.csv"
retrieveRowsCallback="process" xlsFileName="意向跟进.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{TASK_ID:'${row.TASK_ID}'});refreshConextmenu()" onclick="selectRow(this,{TASK_ID:'${row.TASK_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="CUST_NAME" title="名称"   />
	<ec:column width="100" property="CUST_INDUSTRY" title="行业"   mappingItem="CUST_INDUSTRY"/>
	<ec:column width="100" property="CUST_SCALE" title="规模"   mappingItem="CUST_SCALE"/>
	<ec:column width="100" property="CUST_NATURE" title="性质"   mappingItem="CUST_NATURE"/>
	<ec:column width="100" property="CUST_STATE" title="状态"   mappingItem="CUST_STATE"/>
	<ec:column width="120" property="TASK_FOLLOW_STATE" title="跟进状态" mappingItem="TASK_FOLLOW_STATE" />
</ec:row>
</ec:table>
<input type="hidden" name="TASK_ID" id="TASK_ID" value="" />
<input type="hidden" name="CUST_ID" id="CUST_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
<input type="hidden" name="ids" id="ids" value="<%=pageBean.inputValue("ids")%>" />
<input type="hidden" name="TASK_REVIEW_STATE" id="TASK_REVIEW_STATE" value="<%=pageBean.inputValue("TASK_REVIEW_STATE")%>" />
<script language="JavaScript">
setRsIdTag('TASK_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
