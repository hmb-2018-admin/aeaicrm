<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>订单管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveMasterRecord(){
	if (validate()){
		if (ele("currentSubTableId")){
			var subTableId = $("#currentSubTableId").val();
			if (!checkEntryRecords(subTableId)){
				return;
			}
		}
		showSplash();
		postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
			if ("fail" != responseText){
				$('#operaType').val('update');
				$('#ORDER_ID').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});
	}
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
function deleteEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要删除该条记录吗？',function(r){
		if(r){
			$('#currentSubTableId').val(subTableId);
			doSubmit({actionType:'deleteEntryRecord'});	
		}
	});
}
function checkEntryRecords(subTableId){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
	return result;
}
var insertSubRecordBox;
function insertSubRecordRequest(title,handlerId){
	if (!insertSubRecordBox){
		insertSubRecordBox = new PopupBox('insertSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=insert&ORDER_ID='+$('#ORDER_ID').val();
	insertSubRecordBox.sendRequest(url);	
}
var copySubRecordBox;
function copySubRecordRequest(title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!copySubRecordBox){
		copySubRecordBox = new PopupBox('copySubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=copy&'+subPKField+'='+$("#"+subPKField).val();
	copySubRecordBox.sendRequest(url);	
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}
function deleteSubRecord(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	jConfirm('确认要删除该条记录吗？',function(r){
		if(r){
			doSubmit({actionType:'deleteSubRecord'});
		}
	});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top:7px;">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeSubTable('_base')">基础信息</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
 <div class="newarticle1" onclick="changeSubTable('OrderEntry')">产品清单</div>
<%}%>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">
<div style="margin:2px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td></aeai:previlege>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
  <th width="100" nowrap>商机名称</th>
  <td><input name="OPP_ID_NAME" type="text" class="text"	id="OPP_ID_NAME"	value="<%=pageBean.inputValue("OPP_ID_NAME")%>" size="24" style="width: 302px" readonly="readonly" label="商机名称" /> 
    <input id="OPP_ID" label="商机名称"  name="OPP_ID" type="hidden"	value="<%=pageBean.inputValue("OPP_ID")%>" size="24" class="text" />
  </td>
  <tr>
	<th width="100" nowrap>客户名称</th>
	<td><input name="ORDER_NAME" type="text" class="text" id="ORDER_NAME" value="<%=pageBean.inputValue("ORDER_NAME")%>" size="24" style="width: 302px" readonly="readonly" label="客户名称" />
</td>
</tr>
<tr>
	<th width="100" nowrap>负责人</th>
	<td><input id="ORDER_CHIEF" label="负责人" name="ORDER_CHIEF" type="text" value="<%=pageBean.inputValue("ORDER_CHIEF")%>" size="24" style="width: 302px" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>销售人员</th>
	<td><input name="CLUE_SALESMAN_NAME" type="text" class="text"	id="CLUE_SALESMAN_NAME"	value="<%=pageBean.inputValue("CLUE_SALESMAN_NAME")%>" size="24" style="width: 302px" readonly="readonly" label="销售人员" /> 
		<input id="CLUE_SALESMAN" label="销售人员" name="CLUE_SALESMAN" type="hidden" value="<%=pageBean.inputValue("CLUE_SALESMAN")%>" size="24" class="text" />
	</td>
</tr>
<tr>
	<th width="100" nowrap>交付费用</th>
	<td><input id="ORDER_DELIVERY_COST" label="交付费用" name="ORDER_DELIVERY_COST" type="text" maxlength="8" value="<%=pageBean.inputValue("ORDER_DELIVERY_COST")%>" size="24" style="width: 302px" class="text" readonly="readonly"/>
(单位：万)</td>
</tr>
<tr>
	<th width="100" nowrap>订单费用</th>
	<td><input id="ORDER_COST" label="订单费用" name="ORDER_COST" type="text" value="<%=pageBean.inputValue("ORDER_COST")%>" readonly="readonly" size="24" style="width: 302px" class="text" />
(单位：万)</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td>
	<input id="ORDER_STATE_TEXT" label="状态" name="ORDER_STATE_TEXT" type="text" value="<%=pageBean.selectedText("ORDER_STATE")%>" size="24" style="width: 302px"  class="text" readonly="readonly"/>
	<input id="ORDER_STATE" label="状态" name="ORDER_STATE" type="hidden" value="<%=pageBean.selectedValue("ORDER_STATE")%>" />
   </td>
</tr>
<tr>
  <th width="100" nowrap>创建人</th>
  <td><input name="ORDER_CREATER_NAME" type="text" class="text"	id="ORDER_CREATER_NAME"	value="<%=pageBean.inputValue("ORDER_CREATER_NAME")%>" size="24" style="width: 302px"	readonly="readonly" label="创建人" /> 
    <input id="ORDER_CREATER" label="创建人"  name="ORDER_CREATER" type="hidden"	value="<%=pageBean.inputValue("ORDER_CREATER")%>" size="24" class="text" />
  </td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input id="ORDER_CREATE_TIME" label="创建时间" name="ORDER_CREATE_TIME" type="text" value="<%=pageBean.inputTime("ORDER_CREATE_TIME")%>" size="24" style="width: 302px" readonly="readonly" class="text" />
</td>
</tr>
<%if(pageBean.getBoolValue("showConfirm")){ %>
<tr>
  <th width="100" nowrap>确认人</th>
  <td><input name="ORDER_CONFIRM_NAME" type="text" class="text"	id="ORDER_CONFIRM_NAME"	value="<%=pageBean.inputValue("ORDER_CONFIRM_NAME")%>" size="24" style="width: 302px"	readonly="readonly" label="确认人" /> 
    <input id="ORDER_CONFIRM_PERSON" label="确认人"  name="ORDER_CONFIRM_PERSON" type="hidden"	value="<%=pageBean.inputValue("ORDER_CONFIRM_PERSON")%>" size="24" class="text" />
  </td>
</tr>
<tr>
	<th width="100" nowrap>确认时间</th>
	<td><input id="ORDER_CONFIRM_TIME" label="确认时间" name="ORDER_CONFIRM_TIME" type="text" value="<%=pageBean.inputDate("ORDER_CONFIRM_TIME")%>" readonly="readonly" size="24" style="width: 302px" class="text" />
</td>
</tr>
<%} %>
</table>
</div>
</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<%if ("OrderEntry".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1"></table>
</div>
<div style="margin:2px;">
<%
List param1Records = (List)pageBean.getAttribute("OrderEntryRecords");
pageBean.setRsList(param1Records);
%>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="产品清单.csv"
retrieveRowsCallback="process" xlsFileName="产品清单.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="auto"
>
<ec:row styleClass="odd">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="ENTRY_ORDER_PRODUCT" title="预定产品"   />
	<ec:column width="100" property="ENTRY_PRODUCT_MODEL" title="产品型号"   />
	<ec:column width="100" property="ENTRY_NUMBER" title="数量"   />
	<ec:column width="100" property="ENTRY_UNIT_PRICE" title="单价"   />
	<ec:column width="100" property="ENTRY_DISCOUNT" title="折扣"   />
	<ec:column width="100" property="ENTRY_REAL_PRICE" title="实际价格"   />
</ec:row>
</ec:table>
</div>
</div>
<input type="hidden" name="ENTRY_ID" id="ENTRY_ID" value=""/>
<script language="javascript">
setRsIdTag('ENTRY_ID');
</script>
<%}%>


<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
<script language="javascript">
$("#Layer0").hide();
</script>
<%}%>
<%}%>
<script language="javascript">
new Tab('tab','tabHeader','Layer',<%=currentSubTableIndex%>);
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
$("#Layer0").hide();
<%}%>
</script>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="ORDER_ID" name="ORDER_ID" value="<%=pageBean.inputValue("ORDER_ID")%>" />
</div>
</form>
<script language="javascript">
requiredValidator.add("OPP_ID");
requiredValidator.add("ORDER_CHIEF");
numValidator.add("ORDER_DELIVERY_COST");
numValidator.add("ORDER_COST");
initDetailOpertionImage();
$(function(){
	resetTabHeight(80);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
