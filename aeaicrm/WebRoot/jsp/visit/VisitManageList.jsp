<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>拜访记录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function openRequest(actionType)
{	
	var theForm = ele(formTagId);
	if ((actionType == confirmRequestActionValue || actionType == confirmRequestActionValue 
		 || actionType == viewDetailActionValue) && !isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	showSplash(waitMsg);
	$("#"+actionTypeTagId).val(actionType);
	theForm.target="_blank"
	theForm.submit();
	theForm.target="";
	$("#"+actionTypeTagId).val("");
	hideSplash();
}
function goToBackList(){
	parent.closeBox();
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="create"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td></aeai:previlege>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input  value="&nbsp;" title="编辑" type="button"  id="editImgBtn" class="editImgBtn" />编辑</td></aeai:previlege>
   <aeai:previlege code="visitAgain"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="再次拜访" type="button" id="copyImgBtn" class="copyImgBtn" />再次拜访</td></aeai:previlege> 
   <aeai:previlege code="generateClue"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx"  align="center" onclick="doRequest('doCreateClueAction')" ><input id="createCLueBtn" value="&nbsp;"type="button" class="businessImgBtn"  title="生成商机" />生成商机</td></aeai:previlege> 
   <aeai:previlege code="confirm"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="R" align="center" onclick="doRequest('confirmRequest')"><input value="&nbsp;" title="提交" type="button"  id="confirm" class="submitImgBtn" />提交</td></aeai:previlege>
   <aeai:previlege code="reConfirm"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="R" align="center" onclick="doRequest('counterConfirmRequest')"><input id="confirmCounter" value="&nbsp;" title="反提交" type="button" class="reSubmittedImgBtn" />反提交</td></aeai:previlege>
   <aeai:previlege code="detail"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td></aeai:previlege>   
   <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input  value="&nbsp;" title="删除" type="button" id="delete" class="delImgBtn" />删除</td></aeai:previlege>
   <aeai:previlege code="back"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBackList();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td></aeai:previlege>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr>
  <td>
&nbsp;拜访日期
<input id="sdate" label="拜访日期" name="sdate" type="text" value="<%=pageBean.inputDate("sdate")%>" size="10" class="text" /><img id="sdatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
<input id="edate" label="拜访日期" name="edate" type="text" value="<%=pageBean.inputDate("edate")%>" size="10" class="text" /><img id="edatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;拜访人员<input id="visitUserName" label="拜访人员" name="visitUserName" type="text" value="<%=pageBean.inputValue("visitUserName")%>" size="8" class="text" ondblclick="emptyText('visitUserName')" />
&nbsp;拜访类型<select id="VISIT_TYPE" label="状态" name="VISIT_TYPE" class="select" onchange="doQuery()"><%=pageBean.selectValue("VISIT_TYPE")%></select>
&nbsp;沟通效果<select id="VISIT_EFFECT" label="沟通效果" name="VISIT_EFFECT" class="select" onchange="doQuery()"><%=pageBean.selectValue("VISIT_EFFECT")%></select>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
useAjax="true" sortable="true"
items="pageBean.rsList"
retrieveRowsCallback="process"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,{VISIT_ID:'${row.VISIT_ID}',CUST_VISIT_CATEGORY:'${row.CUST_VISIT_CATEGORY}'});controlUpdateBtn('${row.VISIT_STATE}','${row.CUST_VISIT_CATEGORY}');refreshConextmenu()" onclick="selectRow(this,{VISIT_ID:'${row.VISIT_ID}',CUST_VISIT_CATEGORY:'${row.CUST_VISIT_CATEGORY}'});controlUpdateBtn('${row.VISIT_STATE}','${row.CUST_VISIT_CATEGORY}');">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="VISIT_TYPE" title="拜访类型"   mappingItem="VISIT_TYPE"/>
	<ec:column width="100" property="VISIT_CUST_ID_NAME" title="客户名称"   />
	<ec:column width="100" property="VISIT_USER_NAME" title="拜访人员"   />
	<ec:column width="100" property="VISIT_DATE" title="拜访时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="100" property="VISIT_STATE" title="状态"   mappingItem="VISIT_STATE"/>
	<ec:column width="100" property="VISIT_EFFECT" title="沟通效果"  mappingItem="VISIT_EFFECT"/>
    <ec:column width="100" property="VISIT_FILL_TIME" title="填写时间" cell="date" format="yyyy-MM-dd HH:mm" />
    <ec:column width="50" property="CUST_VISIT_CATEGORY" title="拜访类别" mappingItem="CUST_VISIT_CATEGORY"/>
</ec:row>
</ec:table>
<input type="hidden" name="VISIT_ID" id="VISIT_ID" value="" />
<input type="hidden" name="CUST_VISIT_CATEGORY" id="CUST_VISIT_CATEGORY" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="custId" id="custId" value="<%=pageBean.inputValue("custId")%>" />
<script language="JavaScript">
setRsIdTag('VISIT_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
initCalendar('sdate','%Y-%m-%d','sdatePicker');
initCalendar('edate','%Y-%m-%d','edatePicker');
datetimeValidators[0].set("yyyy-MM-dd").add("sdate");
datetimeValidators[1].set("yyyy-MM-dd").add("edate");
function controlUpdateBtn(stateResult,visitCategory){
	if(stateResult =='init'){
		enableButton("confirm");
		enableButton("delete");
		disableButton("confirmCounter");
		disableButton("createCLueBtn");
		enableButton("editImgBtn");
		
	}else if(stateResult =='Submit'){
		disableButton("confirm");
		disableButton("delete");
		enableButton("confirmCounter");
		enableButton("createCLueBtn");
		disableButton("editImgBtn");
	}
	
	if(visitCategory == 'PRO_CUST'){
		disableButton("confirm");
		disableButton("delete");
		disableButton("confirmCounter");
		disableButton("createCLueBtn");
		disableButton("editImgBtn");
		disableButton("copyImgBtn");
	}
}
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
