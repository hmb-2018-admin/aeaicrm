package com.agileai.crm.module.customer.service;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;
import com.agileai.util.StringUtil;

public class CustomerGroupManageImpl
        extends TreeAndContentManageImpl
        implements CustomerGroupManage {
	protected String idField = "GRP_ID";
	protected String nameField = "GRP_NAME";
	protected String parentIdField = "GRP_SUP_ID";
	
    public CustomerGroupManageImpl() {
        super();
        this.columnIdField = "GRP_ID";
        this.columnParentIdField = "GRP_SUP_ID";
        this.columnSortField = "GRP_SORT";
    }
    protected int getNewMaxSort(String parentId){
		int result = 0;
		String statementId = this.sqlNameSpace+"."+"queryMaxSortId";
		DataRow row = this.daoHelper.getRecord(statementId, parentId);
		String maxMenuSort = row.stringValue("MAX_"+columnSortField);
		if (!StringUtil.isNullOrEmpty(maxMenuSort)){
			result = Integer.parseInt(maxMenuSort)+1;
		}else{
			result =1;
		}
		return result;
	}
    public void insertChildRecord(DataParam param) {
		String parentId = param.get(idField);
		String newMenuSort = String.valueOf(this.getNewMaxSort(parentId));
		param.put("CHILD_"+columnSortField,newMenuSort);
		param.put("CHILD_"+idField,KeyGenerator.instance().genKey());
		param.put("CHILD_"+parentIdField,param.get(idField));
		String statementId = this.sqlNameSpace+"."+"insertTreeRecord";
		this.daoHelper.insertRecord(statementId, param);
	}
    public void changeTreeSort(String currentId, boolean isUp) {
		String statementId = this.sqlNameSpace+"."+"queryCurLevelRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, currentId);
		DataRow curRow = null;
		String curSort = null;
		if (isUp){
			DataRow beforeRow = null;
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String tempMenuId = row.stringValue(columnIdField);
				if (currentId.equals(tempMenuId)){
					curRow = row;
					beforeRow = records.get(i-1);
					break;
				}
			}
			curSort = curRow.stringValue(columnSortField);
			String beforeSort = beforeRow.stringValue(columnSortField);;
			curRow.put(columnSortField,beforeSort);
			beforeRow.put(columnSortField,curSort);
			this.updateTreeRecord(curRow.toDataParam());
			this.updateTreeRecord(beforeRow.toDataParam());
		}else{
			DataRow nextRow = null;
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String tempMenuId = row.stringValue(columnIdField);
				if (currentId.equals(tempMenuId)){
					curRow = row;
					nextRow = records.get(i+1);
					break;
				}
			}
			curSort = curRow.stringValue(columnSortField);
			String nextSort = nextRow.stringValue(columnSortField);
			curRow.put(columnSortField,nextSort);
			nextRow.put(columnSortField,curSort);
			this.updateTreeRecord(curRow.toDataParam());
			this.updateTreeRecord(nextRow.toDataParam());
		}
	}
	@Override
	public DataRow getGroupRecord(DataParam param) {
		String statementId = sqlNameSpace+".getGroupRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
	@Override
	public List<DataRow> findTreeGroupRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findTreeGroupRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}
	@Override
	public List<DataRow> querySalePickTreeRecords(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"querySalePickTreeRecords";
		List<DataRow> rsList = this.daoHelper.queryRecords(statementId, param);
		return rsList;
	}
}
