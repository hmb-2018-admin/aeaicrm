package com.agileai.crm.module.mytasks.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.crm.cxmodule.FollowUpManage;
import com.agileai.crm.cxmodule.OppInfoManage;
import com.agileai.crm.cxmodule.OrderInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class FollowUpManageEditHandler
        extends MasterSubEditMainHandler {
    public FollowUpManageEditHandler() {
        super();
        this.listHandlerClass = FollowUpManageListHandler.class;
        this.serviceId = buildServiceId(FollowUpManage.class);
        this.baseTablePK = "TASK_ID";
        this.defaultTabId = "_base";
    }
    
	public ViewRenderer prepareDisplay(DataParam param) {
		if(!"00000000-0000-0000-00000000000000000".equals(param.get("TASK_REVIEW_ID"))){
			DataRow dataRow = getService().getTaskReviewRecord(param);
			String taskReviewState = dataRow.getString("TASK_REVIEW_STATE");
			this.setAttribute("TASK_REVIEW_STATE", taskReviewState);
		}
		
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getMasterRecord(param);
			this.setAttributes(record);
			DataRow dataRow = getService().getCustPhoneInfoRecord(new DataParam("CUST_ID",record.get("CUST_ID")));
			String phone = "";
			if(dataRow != null){
				phone = dataRow.getString("CONT_NAME") +":"+ dataRow.getString("CONT_PHONE");
			}
			
			this.setAttribute("CUST_PHONE", phone);
			String taskFollowState = (String) record.get("TASK_FOLLOW_STATE");
			if("NoFollowUp".equals(taskFollowState)){
				this.setAttribute("buttonName", "跟进");
			}else{
				this.setAttribute("buttonName", "再跟进");
			}
		}
		String currentSubTableId = param.get("currentSubTableId",defaultTabId);
		if (!currentSubTableId.equals(MasterSubService.BASE_TABLE_ID)){
			String subRecordsKey = currentSubTableId + "Records";
			if (!this.getAttributesContainer().containsKey(subRecordsKey)){
				List<DataRow> subRecords = getService().findSubRecords(currentSubTableId, param);
				this.setAttribute(currentSubTableId+"Records", subRecords);
			}
		}
		this.setAttribute("currentSubTableId", currentSubTableId);
		this.setAttribute("currentSubTableIndex", getTabIndex(currentSubTableId));
		String operateType = param.get(OperaType.KEY);
		this.setOperaType(operateType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
		setAttribute("CUST_INDUSTRY", FormSelectFactory.create("CUST_INDUSTRY")
				.addSelectedValue(getOperaAttributeValue("CUST_INDUSTRY", "")));
		setAttribute("CUST_SCALE", FormSelectFactory.create("CUST_SCALE")
				.addSelectedValue(getOperaAttributeValue("CUST_SCALE", "")));
		setAttribute("CUST_NATURE", FormSelectFactory.create("CUST_NATURE")
				.addSelectedValue(getOperaAttributeValue("CUST_NATURE", "")));
		setAttribute("CUST_STATE", FormSelectFactory.create("CUST_STATE")
				.addSelectedValue(getOperaAttributeValue("CUST_STATE", "init")));
		setAttribute("CUST_LEVEL", FormSelectFactory.create("CUST_LEVEL")
				.addSelectedValue(getOperaAttributeValue("CUST_LEVEL", "")));
		setAttribute("CUST_PROGRESS_STATE", FormSelectFactory.create("CUST_PROGRESS_STATE")
				.addSelectedValue(getOperaAttributeValue("CUST_PROGRESS_STATE", "")));
		initMappingItem("PROCUST_VISIT_EFFECT", 
				FormSelectFactory.create("VISIT_EFFECT").getContent());
		initMappingItem("PROCUST_VISIT_TYPE", 
				FormSelectFactory.create("VISIT_TYPE").getContent());
    }
    
    public ViewRenderer doGoBackAction(DataParam param){
    	return new RedirectRenderer(getHandlerURL(listHandlerClass)+"&TASK_REVIEW_ID="+param.get("TASK_REVIEW_ID"));
    }
    
    public ViewRenderer doCheckBusinessOrdersAction(DataParam param){
    	String rspText = "";
    	String custId = param.getString("CUST_ID");
    	OppInfoManage oppInfoManage = this.lookupService(OppInfoManage.class);
    	List<DataRow> oppRsList = oppInfoManage.findRecords(new DataParam("custId",custId));
    	
    	OrderInfoManage orderInfoManage = this.lookupService(OrderInfoManage.class);
    	List<DataRow> orderRsList = orderInfoManage.findMasterRecords(new DataParam("custId",custId));
    	if(oppRsList.size() == 0 && orderRsList.size() == 0){
    		rspText = SUCCESS;
    	}
    	
    	return new AjaxRenderer(rspText);
    }

    protected String[] getEntryEditFields(String currentSubTableId) {
        List<String> temp = new ArrayList<String>();

        return temp.toArray(new String[] {  });
    }

    protected String getEntryEditTablePK(String currentSubTableId) {
        HashMap<String, String> primaryKeys = new HashMap<String, String>();
        primaryKeys.put("MyCustVisitInfo", "CUST_ID");

        return primaryKeys.get(currentSubTableId);
    }

    protected String getEntryEditForeignKey(String currentSubTableId) {
        HashMap<String, String> foreignKeys = new HashMap<String, String>();
        foreignKeys.put("MyCustVisitInfo", "CUST_ID");

        return foreignKeys.get(currentSubTableId);
    }

    protected FollowUpManage getService() {
        return (FollowUpManage) this.lookupService(this.getServiceId());
    }
}
