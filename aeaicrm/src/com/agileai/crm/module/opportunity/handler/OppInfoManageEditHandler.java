package com.agileai.crm.module.opportunity.handler;

import java.math.BigDecimal;
import java.util.Date;

import com.agileai.crm.cxmodule.OppInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class OppInfoManageEditHandler
        extends StandardEditHandler {
    public OppInfoManageEditHandler() {
        super();
        this.listHandlerClass = OppInfoManageListHandler.class;
        this.serviceId = buildServiceId(OppInfoManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if(OperaType.CREATE.equals(operaType)){
			setAttribute("doEdit8Save", true);
			setAttribute("doAssign", true);
			setAttribute("CUST_ID", param.get("custId"));
		}
		if("update".equals(operaType)){
			DataRow record = getService().getRecord(param);
			setAttribute("doEdit8Save", true);
			this.setAttributes(record);	
		}
		if("detail".equals(operaType)){
			DataRow record = getService().getRecord(param);
			if("0".equals(record.get("OPP_STATE"))){
				setAttribute("doEdit8Save", true);
				setAttribute("doSubmit", true);
				setAttribute("doAssign", true);
			}
			if("1".equals(record.get("OPP_STATE"))){
				setAttribute("doRevokeSubmit", true);
				setAttribute("doConfirm", true);
				setAttribute("doAssign", true);
				setAttribute("doClose",true);
			}
			if("2".equals(record.get("OPP_STATE"))){
				setAttribute("doRevokeConfirm", true);
				setAttribute("readOnly", true);
				setAttribute("doAssign", true);
				setAttribute("doClose",true);
			}
			this.setAttributes(record);	
		}
		if("doDispose".equals(operaType)){
			DataRow record = getService().getRecord(param);
			if("0".equals(record.get("OPP_STATE"))){
				setAttribute("doEdit8Save", true);
				setAttribute("doSubmit", true);
				setAttribute("doAssign", true);
			}
			if("1".equals(record.get("OPP_STATE"))){
				setAttribute("doRevokeSubmit", true);
				setAttribute("doConfirm", true);
				setAttribute("doAssign", true);
				setAttribute("doClose",true);
			}
			if("2".equals(record.get("OPP_STATE"))){
				setAttribute("doRevokeConfirm", true);
				setAttribute("readOnly", true);
				setAttribute("doAssign", true);
				setAttribute("doClose",true);
			}
			this.setAttributes(record);	
		}
		
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
        setAttribute("OPP_STATE",
                     FormSelectFactory.create("OPP_STATE")
                                      .addSelectedValue(getAttributeValue("OPP_STATE",
                                                                               "0")));
		setAttribute("OPP_LEVEL", FormSelectFactory.create("OPP_LEVEL")
				.addSelectedValue(getOperaAttributeValue("OPP_LEVEL", "")));
        User user = (User) this.getUser();
		this.setAttribute("OPP_CREATER_NAME",
				this.getAttribute("OPP_CREATER_NAME", user.getUserName()));
		this.setAttribute("OPP_CREATER",
				this.getAttribute("OPP_CREATER", user.getUserId()));
		String ocrtDate = (String) this.getAttribute("OPP_CREATE_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));
		this.setAttribute("OPP_CREATE_TIME", ocrtDate);
        BigDecimal oppMoney = new BigDecimal("0.00");
		if (this.getAttribute("OPP_EXPECT_INVEST") == null) {
			this.setAttribute("OPP_EXPECT_INVEST", oppMoney);
		}
    }
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			if(param.get("OPP_EXPECT_INVEST").trim().equals("")){
				param.put("OPP_EXPECT_INVEST","0.00");
			}
			getService().createRecord(param);
		}
		if (OperaType.UPDATE.equals(operateType)){
			if(param.get("OPP_EXPECT_INVEST").trim().equals("")){
				param.put("OPP_EXPECT_INVEST","0.00");
			}
			getService().updateRecord(param);	
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	@PageAction
	public ViewRenderer doSubmit(DataParam param) {
		param.put("OPP_STATE","1");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	@PageAction
	public ViewRenderer doRevokeSubmit(DataParam param) {
		param.put("OPP_STATE","0");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}	
	@PageAction
	public ViewRenderer doConfirm(DataParam param) {
		param.put("OPP_STATE","2");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	@PageAction
	public ViewRenderer doRevokeConfirm(DataParam param) {
		param.put("OPP_STATE","0");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	@PageAction
	public ViewRenderer doClose(DataParam param) {
		param.put("OPP_STATE", "3");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	@PageAction
	public ViewRenderer doCreateOppContAction(DataParam param) {
		storeParam(param);
		String url = "OppCreateContEdit";
		return new DispatchRenderer(
				getHandlerURL(url)
						+ "&"
						+ OperaType.KEY
						+ "=doCreateOppContAction&comeFrome=doCreateOppContAction");
	}
    protected OppInfoManage getService() {
        return (OppInfoManage) this.lookupService(this.getServiceId());
    }
}
