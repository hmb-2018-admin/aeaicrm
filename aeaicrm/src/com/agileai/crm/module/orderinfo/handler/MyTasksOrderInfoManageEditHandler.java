package com.agileai.crm.module.orderinfo.handler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.agileai.crm.cxmodule.OrderInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class MyTasksOrderInfoManageEditHandler
        extends MasterSubEditMainHandler {
    public MyTasksOrderInfoManageEditHandler() {
        super();
        this.listHandlerClass = MyTasksOrderInfoManageListHandler.class;
        this.serviceId = buildServiceId(OrderInfoManage.class);
        this.baseTablePK = "ORDER_ID";
        this.defaultTabId = "_base";
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getMasterRecord(param);
			this.setAttributes(record);			
		}
		if("insert".equals(operaType)){
			setAttribute("doEdit8Save", true);
			setAttribute("doConfirm", false);
			setAttribute("doRepealConfirm", false);
		}
		if("update".equals(operaType)){
			DataRow record = getService().getMasterRecord(param);
			if(record.get("ORDER_STATE").equals("0")){
				setAttribute("doEdit8Save", true);
				setAttribute("doSubmit", true);
			}
		}
		if("copy".equals(operaType)){
			setAttribute("ORDER_STATE", "0");
			setAttribute("doEdit8Save", true);
			setAttribute("doSubmit", true);
		}
		if("detail".equals(operaType)){
			DataRow record = getService().getMasterRecord(param);
			if(record.get("ORDER_STATE").equals("0")){
				setAttribute("doEdit8Save", true);
				setAttribute("doSubmit", true);
				setAttribute("doRepealConfirm", false);
				setAttribute("showConfirm", false);
			}if(record.get("ORDER_STATE").equals("1")){
				setAttribute("doEdit8Save", false);
				setAttribute("doConfirm",true);
				setAttribute("doRevokeSubmit",true);
				setAttribute("showConfirm", true);
			}
			if(record.get("ORDER_STATE").equals("2")){
				setAttribute("doEdit8Save", false);
				setAttribute("doConfirm",false);
				setAttribute("doRepealConfirm", true);
				setAttribute("showConfirm", true);
			}
		}
		if("doSubmit".equals(operaType)){
			setAttribute("doEdit8Save", true);
			setAttribute("doSubmit", true);
			DataRow record = getService().getMasterRecord(param);
			this.setAttributes(record);	
		}
		if("doConfirm".equals(operaType)){
			setAttribute("doEdit8Save", true);
			setAttribute("doConfirm", true);
			setAttribute("showConfirm", true);
			DataRow record = getService().getMasterRecord(param);
			this.setAttributes(record);			
			User user = (User) this.getUser();
			this.setAttribute(
					"ORDER_CONFIRM_NAME",
					this.getAttribute("ORDER_CONFIRM_NAME",
							user.getUserName()));
			this.setAttribute("ORDER_CONFIRM_PERSON",
					this.getAttribute("ORDER_CONFIRM_PERSON", user.getUserId()));
				String date = DateUtil.getDateByType(
						DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
				this.setAttribute("ORDER_CONFIRM_TIME", date);
		}
		String currentSubTableId = param.get("currentSubTableId",defaultTabId);
		if (!currentSubTableId.equals(MasterSubService.BASE_TABLE_ID)){
			String subRecordsKey = currentSubTableId + "Records";
			if (!this.getAttributesContainer().containsKey(subRecordsKey)){
				List<DataRow> subRecords = getService().findSubRecords(currentSubTableId, param);
				this.setAttribute(currentSubTableId+"Records", subRecords);
			}
		}
		this.setAttribute("currentSubTableId", currentSubTableId);
		this.setAttribute("currentSubTableIndex", getTabIndex(currentSubTableId));
		String operateType = param.get(OperaType.KEY);
		this.setOperaType(operateType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
    	User user = (User) this.getUser();
		this.setAttribute("ORDER_CREATER_NAME",
				this.getAttribute("ORDER_CREATER_NAME", user.getUserName()));
		this.setAttribute("ORDER_CREATER",
				this.getAttribute("ORDER_CREATER", user.getUserId()));
		String ccrtDate = (String) this.getAttribute("ORDER_CREATE_TIME",
				DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL,
						new Date()));
		  setAttribute("ORDER_STATE",
                  FormSelectFactory.create("ORDER_STATE")
                                   .addSelectedValue(getAttributeValue("ORDER_STATE",
                                                                            "0")));
		this.setAttribute("ORDER_CREATE_TIME", ccrtDate);
		BigDecimal orderMoney = new BigDecimal("0.00");
		if (this.getAttribute("ORDER_DELIVERY_COST") == null) {
			this.setAttribute("ORDER_DELIVERY_COST", orderMoney);
		}if (this.getAttribute("ORDER_COST") == null) {
			this.setAttribute("ORDER_COST", orderMoney);
		}
		
    }
    @PageAction
	public ViewRenderer doConfirm(DataParam param) {
    	User user = (User) this.getUser();
    	param.put("ORDER_CONFIRM_PERSON",user.getUserId());
		param.put("ORDER_STATE","1");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
    @PageAction
	public ViewRenderer doRepealConfirm(DataParam param) {
    	User user = (User) this.getUser();
    	param.put("ORDER_CONFIRM_PERSON",user.getUserId());
		param.put("ORDER_STATE","0");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
    public ViewRenderer doSaveMasterRecordAction(DataParam param) {
		String operateType = param.get(OperaType.KEY);
		String responseText = "fail";
		if (OperaType.CREATE.equals(operateType)){
			getService().createMasterRecord(param);
			responseText = param.get(baseTablePK);
		}
		if (OperaType.UPDATE.equals(operateType)) {
			BigDecimal orderMoney = new BigDecimal("0.00");
			if(param.get("ORDER_DELIVERY_COST").trim().equals("")){
				param.put("ORDER_DELIVERY_COST",orderMoney);
			}
			getService().updateMasterRecord(param);
			saveSubRecords(param);
			responseText = param.get(baseTablePK);
		}
		String masterRecordId = param.get("ORDER_ID");
		String entryId = param.get("ENTRY_ID");
		this.getService().computeTotalMoney(masterRecordId,entryId);
		return new AjaxRenderer(responseText);
	}
    
    @PageAction
	public ViewRenderer doSubmit(DataParam param) {
		param.put("ORDER_STATE","1");
		getService().changeStateRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
    
    protected String[] getEntryEditFields(String currentSubTableId) {
        List<String> temp = new ArrayList<String>();

        return temp.toArray(new String[] {  });
    }

    protected String getEntryEditTablePK(String currentSubTableId) {
        HashMap<String, String> primaryKeys = new HashMap<String, String>();
        primaryKeys.put("OrderEntry", "ENTRY_ID");

        return primaryKeys.get(currentSubTableId);
    }

    protected String getEntryEditForeignKey(String currentSubTableId) {
        HashMap<String, String> foreignKeys = new HashMap<String, String>();
        foreignKeys.put("OrderEntry", "ORDER_ID");

        return foreignKeys.get(currentSubTableId);
    }

    protected OrderInfoManage getService() {
        return (OrderInfoManage) this.lookupService(this.getServiceId());
    }
}
