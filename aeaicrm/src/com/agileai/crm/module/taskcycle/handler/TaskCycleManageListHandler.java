package com.agileai.crm.module.taskcycle.handler;

import com.agileai.crm.cxmodule.TaskCycleManage;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;

public class TaskCycleManageListHandler
        extends StandardListHandler {
    public TaskCycleManageListHandler() {
        super();
        this.editHandlerClazz = TaskCycleManageEditHandler.class;
        this.serviceId = buildServiceId(TaskCycleManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }

    protected TaskCycleManage getService() {
        return (TaskCycleManage) this.lookupService(this.getServiceId());
    }
}
