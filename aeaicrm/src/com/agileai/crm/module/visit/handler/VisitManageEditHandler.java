package com.agileai.crm.module.visit.handler;

import java.math.BigDecimal;
import java.util.Date;

import com.agileai.crm.cxmodule.VisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class VisitManageEditHandler extends StandardEditHandler {
	public VisitManageEditHandler() {
		super();
		this.listHandlerClass = VisitManageListHandler.class;
		this.serviceId = buildServiceId(VisitManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		User user = (User) this.getUser();
		if ("confirm".equals(operaType)) {
			setAttribute("doDetail", false);
			setAttribute("doConfirm", true); 
			setAttribute("doConfirmCounter",false); 
			if (!isReqRecordOperaType(operaType)) {
				DataRow record = getService().getRecord(param);
				this.setAttributes(record);
				this.setAttribute("VISIT_CONFIRM_NAME", user.getUserName());
				this.setAttribute("VISIT_CONFIRM_ID", user.getUserId());
				if (this.getAttribute("VISIT_CONFIRM_TIME") == null) {
					String date = DateUtil.getDateByType(
							DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
					this.setAttribute("VISIT_CONFIRM_TIME", date);
				}
			}
		}
		
		if("counterConfirm".equals(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);
			setAttribute("doDetail", false); 
			setAttribute("doConfirm",false); 
			setAttribute("doConfirmCounter",true); 
		}
		
		if("update".equals(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);
			setAttribute("doCreateCLue", false);
			setAttribute("doDetail", true);
			setAttribute("doConfirm", false); 
			setAttribute("doConfirmCounter",false); 
		}
		
		if("detail".equals(operaType)){
			String visitCategory = param.get("CUST_VISIT_CATEGORY");
			if("PRO_CUST".equals(visitCategory)){
				DataRow record = getService().getProRecord(param);
				this.setAttributes(record);	
				setAttribute("displayTable", true);
			}else{
				DataRow record = getService().getRecord(param);
				this.setAttributes(record);	
				if("init".equals(record.get("VISIT_STATE"))){
					setAttribute("doDetail", true); 
					setAttribute("doCreateCLue", false);
					setAttribute("doConfirm", true); 
					setAttribute("doConfirmCounter",false); 
				}else if("Submit".equals(record.get("VISIT_STATE"))){
					setAttribute("doCreateCLue", true);
					setAttribute("doDetail", false); 
					setAttribute("doConfirmCounter",true);
					setAttribute("doConfirm", false); 
				}
			}
		}
		if("copy".equals(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);
			record.put("VISIT_FILL_ID", user.getUserId());
			record.put("VISIT_FILL_NAME", user.getUserName());
			Date currentDate = new Date();
			String visitDate = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, currentDate);
			record.put("VISIT_DATE",visitDate);
			record.put("VISIT_FILL_TIME", currentDate);
			setAttribute("VISIT_DATE",currentDate );
			setAttributes(record);
			setAttribute("VISIT_FILL_NAME", user.getUserName());
			setAttribute("VISIT_FILL_ID", user.getUserId());
			setAttribute("VISIT_FILL_TIME", new Date());
			setAttribute("VISIT_STATE", "init");
			setAttribute("VISIT_EFFECT", "");
			setAttribute("VISIT_CONFIRM_ID", "");
			setAttribute("VISIT_CONFIRM_NAME", "");
			setAttribute("VISIT_CONFIRM_TIME", "");
			
			setAttribute("doDetail", true);
			setAttribute("doConfirm", false); 
			setAttribute("doConfirmCounter",false); 
			setAttribute("doCreateCLue", false);
		}
		if (OperaType.CREATE.equals(operaType)){
			setAttribute("VISIT_COST", "0.00");
			setAttribute("doDetail", true); 
			setAttribute("doConfirm", false); 
			setAttribute("doConfirmCounter",false);
			setAttribute("doCreateCLue", false);
			this.setAttribute("VISIT_CUST_ID", param.get("custId"));
			this.setAttribute("VISIT_USER_ID",user.getUserId());
			this.setAttribute("VISIT_USER_NAME",user.getUserName());
			this.setAttribute("VISIT_FILL_NAME",user.getUserName());
			this.setAttribute("VISIT_FILL_ID",user.getUserId());
			String date = DateUtil.getDateByType(
					DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
			this.setAttribute("VISIT_FILL_TIME",
					this.getAttribute("VISIT_FILL_TIME",date));

			Date visitDate = (Date) new Date();
			setAttribute("VISIT_DATE",visitDate );
		}
		
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

	protected void processPageAttributes(DataParam param) {
		setAttribute("VISIT_TYPE", FormSelectFactory.create("VISIT_TYPE")
				.addSelectedValue(getOperaAttributeValue("VISIT_TYPE", "")));
		setAttribute("VISIT_EFFECT", FormSelectFactory.create("VISIT_EFFECT")
				.addSelectedValue(getOperaAttributeValue("VISIT_EFFECT", "")));
		setAttribute(
				"VISIT_RECEPTION_SEX",
				FormSelectFactory.create("USER_SEX").addSelectedValue(
						getOperaAttributeValue("VISIT_RECEPTION_SEX", "M")));
		setAttribute(
				"VISIT_STATE",
				FormSelectFactory.create("CUST_STATE").addSelectedValue(
						getOperaAttributeValue("VISIT_STATE", "init")));
        setAttribute("CUST_VISIT_CATEGORY",
                FormSelectFactory.create("CUST_VISIT_CATEGORY")
                                 .addSelectedValue(getOperaAttributeValue("CUST_VISIT_CATEGORY",
                                                                          "")));
        setAttribute("PROCUST_VISIT_EFFECT",
                FormSelectFactory.create("VISIT_EFFECT")
                                 .addSelectedValue(getOperaAttributeValue("PROCUST_VISIT_EFFECT",
                                                                          "")));
   setAttribute("PROCUST_VISIT_TYPE",
           FormSelectFactory.create("VISIT_TYPE")
                            .addSelectedValue(getOperaAttributeValue("PROCUST_VISIT_TYPE",
                                                                     "")));
	}
	
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			BigDecimal orderMoney = new BigDecimal("0.00");
			if(param.get("VISIT_COST").trim().equals("")){
				param.put("VISIT_COST",orderMoney);
			}
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			BigDecimal orderMoney = new BigDecimal("0.00");
			if(param.get("VISIT_COST").trim().equals("")){
				param.put("VISIT_COST",orderMoney);
			}
			getService().updateRecord(param);	
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	
	@PageAction
	public ViewRenderer confirm(DataParam param) {
		User user = (User) this.getUser();
		param.put("VISIT_STATE", "Submit");
		param.put("VISIT_CONFIRM_ID",user.getUserId());
		param.put("VISIT_CONFIRM_TIME", new Date());
		getService().updateConfirmStateInfoRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	
	@PageAction
	public ViewRenderer confirmCounter(DataParam param) {
		param.put("VISIT_STATE", "init");
		param.put("VISIT_CONFIRM_ID","");
		param.put("VISIT_CONFIRM_TIME","");
		getService().updateConfirmStateInfoRecord(param);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	
	 @PageAction
	 public ViewRenderer doCreateClueAction(DataParam param) {
			storeParam(param);
			String url = "VisitCreateClueEdit";
			return new DispatchRenderer(getHandlerURL(url) + "&"
					+ OperaType.KEY + "=doCreateClueAction&comeFrome=doCreateClueAction");
		}
	
	protected VisitManage getService() {
		return (VisitManage) this.lookupService(this.getServiceId());
	}
}
